# Classe principal
class GameController < ApplicationController
  include GameHelper
  skip_before_action :verify_authenticity_token
  def index; end

  def move
    board, x, y, color = state_params
    piece_name = params[:piece][1]
    piece = PieceMover.new(piece_name)
                      .piece_class.new(board,x,y,color)
    valid = piece.valid_moves.include? params[:target]
    puts(piece.valid_moves)
    render json: {valid: valid}
  end

  def state_params
    [ board  = params[:oldPos], x = coord_mapping[params[:source][0]],
      y = params[:source][1].to_i,
      color = params[:piece][0] ]
  end



end
