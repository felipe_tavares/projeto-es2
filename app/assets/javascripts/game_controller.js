var onDrop = function(source, target, piece, newPos, oldPos, orientation) {
  //console.log("Source: " + source);
  //console.log("Target: " + target);
  //console.log("Piece: " + piece);
  //console.log("New position: " + ChessBoard.objToFen(newPos));
  //console.log("Old position: " + ChessBoard.objToFen(oldPos));
  //console.log("Orientation: " + orientation);
  //console.log("--------------------");
  var result = null
   $.ajax({
    url: "/game/move",
    type: "POST",
    async: false,
    data: {source: source, target:target, piece: piece, newPos: newPos, oldPos:oldPos},
    success: function (data) {
      result = data
     }
    });
  if(!result["valid"]) return 'snapback'
};
$( document ).on('turbolinks:load', function() {
  var cfg = {
    draggable: true,
    dropOffBoard: 'snapback',
    position: 'start',
    onDrop: onDrop
  };
  var board1 = ChessBoard('board1',cfg);
})
