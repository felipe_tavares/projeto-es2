class Pawn < Piece
  attr_accessor :board, :x, :y, :color
  MOVES = {
    one_step: [0, 1],
    double_step: [0, 2],
    right_diagonal: [1, 1],
    left_diagonal: [-1, 1]
  }

  def initialize(board, x, y, color)
    @board = board
    @x = x
    @y = y
    @color = color
    @moves = MOVES 
    super
  end


  def valid_moves
    @possible_moves = []
    plays = offsets
    plays.each do |k,v|
      temp_x = x+ v[0]
      temp_y = y + v[1]
      if k == :one_step 
        @possible_moves << [temp_x,temp_y]
      end
      if k == :double_step 
        # verifica se os peoes estao nas primeiras fileiras
        @possible_moves << [temp_x,temp_y] if y == 2 && color = 'w'
        @possible_moves << [temp_x,temp_y] if y == 7 && color = 'b'
      end
      #@possible_moves << [temp_x,temp_y]
    end
    convert_to_chess_notation @possible_moves
  end

end
