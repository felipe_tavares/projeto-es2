module Slideable
  include GameHelper

  def valid_moves
    @possible_moves = []

    @moves.each do |move|
      x = @x + move[0]
      y = @y + move[1]
      chess_board_x = inverse_coord_mapping[x]
      chess_board_coord = "#{chess_board_x}#{y}"

      loop do
        break if invalid_coords(x,y)
        chess_board_x = inverse_coord_mapping[x]
        chess_board_coord = "#{chess_board_x}#{y}"
        @possible_moves << [x, y] if board[chess_board_coord].nil? || board[chess_board_coord][0] != @color
        break if !board[chess_board_coord].nil?
        x += move[0]
        y += move[1]
        puts [x,y]
      end
    end
    convert_to_chess_notation @possible_moves
  end
end
