class Knight < Piece
  attr_accessor :board, :x, :y, :color
  attr_reader :moves

  MOVES = [ [-1, -2] , [-2, -1], [-2, 1], [-1, 2], [1, -2], [2, -1], [2, 1], [1, 2] ]
  def initialize(board, x, y, color)
    @board = board
    @x = x
    @y = y
    @color = color
    @moves = MOVES 
    super
  end

  def valid_moves
    @possible_moves = []
    moves.each do |v|
      temp_x = x + v[0]
      temp_y = y + v[1]
      @possible_moves << [temp_x,temp_y]
    end
    convert_to_chess_notation @possible_moves
  end

end
