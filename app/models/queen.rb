class Queen < Piece
  include Slideable
  attr_accessor :board, :x, :y, :color
  attr_reader :moves

  MOVES = [ [1, 1], [1, -1], [-1, 1], [-1, -1], [0, 1], [0, -1], [1, 0], [-1, 0]] 

  def initialize(board, x, y, color)
    @board = board
    @x = x
    @y = y
    @color = color
    @moves = MOVES 
    super
  end


end
