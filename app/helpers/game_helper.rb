module GameHelper
  def coord_mapping
    {"a"=>1, "b"=>2, "c"=>3, "d"=>4, "e"=>5, "f"=>6, "g"=>7, "h"=>8}
  end

  def inverse_coord_mapping
    {1=>"a", 2=>"b", 3=>"c", 4=>"d", 5=>"e", 6=>"f", 7=>"g", 8=>"h"} 
  end

  def convert_to_chess_notation(possible_moves)
    arr = possible_moves.map do |p|
      first_coord = p[0]
      first_coord_chr = inverse_coord_mapping[first_coord]
      "#{first_coord_chr}#{p[1]}"
    end
  end

  def invalid_coords(x,y)
   (x < 0 || y < 0 || x > 8 || y > 8)
  end
end
